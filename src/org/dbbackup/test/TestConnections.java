package org.dbbackup.test;

import org.dbbackup.db.DBConnection;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

public class TestConnections {
    public static void main(String[] args)
            throws SQLException, InterruptedException, IOException {
        DBConnection con = new DBConnection("localhost", "root", "admin");
        con.getDatabases().parallelStream().forEach(System.out::println);
        File backupFile = new File("/home/martin/Documentos/respaldo.sql");
        con.backupDatabase("dbSuncalls", backupFile);

    }
}
