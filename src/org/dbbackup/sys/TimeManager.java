package org.dbbackup.sys;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class TimeManager {

    public static String getCurrentTime() {
        StringBuilder sbTime = new StringBuilder();
        Calendar calendar = new GregorianCalendar();
        sbTime.append(calendar.get(Calendar.YEAR));
        int month = calendar.get(Calendar.MONTH)+1;
        if (month < 10)
            sbTime.append('0');
        sbTime.append(month);

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        if (day < 10)
            sbTime.append('0');
        sbTime.append(day);
        sbTime.append('T');

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        if (hour < 10)
            sbTime.append('0');
        sbTime.append(hour);

        int minutes = calendar.get(Calendar.MINUTE);
        if (minutes < 10)
            sbTime.append('0');
        sbTime.append(minutes);

        int seconds = calendar.get(Calendar.SECOND);
        if (seconds < 10)
            sbTime.append('0');
        sbTime.append(seconds);
        return sbTime.toString();
    }

}
