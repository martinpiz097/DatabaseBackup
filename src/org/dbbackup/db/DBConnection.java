package org.dbbackup.db;

import org.mariadb.jdbc.MariaDbDataSource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class DBConnection {
    private Connection con;
    //private MysqlDataSource mariaDs;
    private MariaDbDataSource mariaDs;
    private String conPassw;

    public DBConnection() {}

    public DBConnection(String host, String user, String passw)
            throws SQLException {
        open(host, user, passw);
    }

    private Statement createStatement() throws SQLException {
        return con.createStatement();
    }

    public void open(String host, String userName, String passw) throws SQLException {
        if (con != null) {
            con.close();
            con = null;
        }
        mariaDs = new MariaDbDataSource();
        mariaDs.setServerName(host);
        mariaDs.setPort(3306);
        mariaDs.setUser(userName);
        mariaDs.setPassword(passw);
        conPassw = passw;
        con = mariaDs.getConnection();
    }

    public void setDatabase(String db) throws SQLException {
        mariaDs.setDatabaseName(db);
        con = mariaDs.getConnection();
    }

    public ResultSet execSelect(String sql) throws SQLException {
        return createStatement().executeQuery(sql);
    }

    public void execSql(String sql) throws SQLException {
        createStatement().execute(sql);
    }

    public LinkedList<String> getDatabases() throws SQLException {
        ResultSet resDbs = execSelect("show databases");
        LinkedList<String> listDatabases = new LinkedList<>();

        while (resDbs.next())
            listDatabases.add(resDbs.getString(1));
        resDbs.close();
        return listDatabases;
    }

    public int backupDatabase(String dbName, File outputFile)
            throws InterruptedException, IOException {
        outputFile.createNewFile();
        StringBuilder sbCmd = new StringBuilder();
        sbCmd.append("mysqldump -u ").append(mariaDs.getUser())
                .append(" --password=").append(conPassw)
                .append(' ').append(dbName);
        //System.out.println("CMD: "+sbCmd.toString());
        Process process = Runtime.getRuntime().exec(sbCmd.toString());
        process.waitFor();
        InputStream inputStream = process.getInputStream();
        FileOutputStream fos = new FileOutputStream(outputFile);

        int read = 0;

        while (read != -1) {
            read = inputStream.read();
            fos.write(read);
        }
        fos.flush();
        fos.close();
        inputStream.close();
        return process.exitValue();
    }

    public double getDatabaseSize(String dbName) throws SQLException {
        StringBuilder sbQuery = new StringBuilder();
        sbQuery.append("SELECT table_schema \"database\", ")
                .append("sum(data_length + index_length)/1024 \"size\" ")
                .append("FROM information_schema.TABLES WHERE table_schema='")
                .append(dbName).append("' GROUP BY table_schema;");
        ResultSet resultSet = execSelect(sbQuery.toString());
        double sizeInKb;

        sizeInKb = resultSet.next() ? resultSet.getDouble("size") : 0;
        resultSet.close();
        return sizeInKb;
    }

    public double getMissing(String dbName, double kbFilter) throws SQLException {
        double dbSize = getDatabaseSize(dbName);
        if (dbSize < kbFilter)
            return kbFilter-dbSize;
        return kbFilter - (dbSize % kbFilter);
    }

}
