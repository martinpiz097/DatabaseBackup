package org.dbbackup.backup;

import org.dbbackup.db.DBConnection;
import org.dbbackup.sys.TimeManager;

import javax.swing.*;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;

public class TBackupMonitor extends Thread {
    private BackupManager manager;
    private DBConnection connection;
    private double currentSize;
    private int currentResidue;
    private double currentMissing;

    private JLabel lblMissing;

    public TBackupMonitor(BackupManager manager, DBConnection connection, JLabel lblMissing) {
        this.manager = manager;
        this.connection = connection;
        this.lblMissing = lblMissing;
        try {
            currentSize = getDatabaseSize();
            currentMissing = getMissing();
            currentResidue = getResidue();
            System.out.println("CurrentSize: "+currentSize);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean shouldBackup() throws SQLException {
        boolean should = currentSize < getDatabaseSize()
                && currentResidue < getResidue();
        System.out.println("CurrentSizeNew: "+getDatabaseSize());
        if (should) {
            currentResidue = getResidue();
            currentMissing = getMissing();
            currentSize = getDatabaseSize();
        }
        return should;
    }

    public void checkNewSize() throws SQLException {
        double newMissing = getMissing();
        if (newMissing != currentMissing) {
            lblMissing.setText(new DecimalFormat("#0.00").format(newMissing));
            lblMissing.updateUI();
        }
    }

    public int getResidue() throws SQLException {
        return (int) (getDatabaseSize() / manager.getActiveBackup().getQuoteInKb());
    }

    public int backupDatabase()
            throws InterruptedException, IOException {
        Backup activeBakcup = manager.getActiveBackup();
        String currentTime = TimeManager.getCurrentTime();
        activeBakcup.setBackupDate(currentTime);
        activeBakcup.addBackupCount();
        activeBakcup.setDumpName(currentTime+'-'
                +activeBakcup.getDatabaseName()+".respaldo");
        return connection.backupDatabase(activeBakcup.getDatabaseName(),
                activeBakcup.getDumpFile());
    }

    public double getDatabaseSize() throws SQLException {
        return connection.getDatabaseSize(manager.getActiveDatabase());
    }

    public double getMissing() throws SQLException {
        Backup activeBackup = manager.getActiveBackup();
        return connection.getMissing(activeBackup.getDatabaseName(),
                activeBackup.getQuoteInKb());
    }

    @Override
    public void run() {
        try {
            while (manager.isActive()) {
                if (shouldBackup()) {
                    backupDatabase();
                    System.out.println("Database respaldada");
                }
                checkNewSize();
                Thread.sleep(60000); // Cambiar por un minuto
                System.out.println("Monitoreando....");
            }
            System.out.println("Monitor cerrado");
        } catch (InterruptedException | IOException | SQLException e) {
            e.printStackTrace();
        }
    }

}
