package org.dbbackup.backup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class BackupProperties {
    private File fileInfo;
    private Properties props;

    // se guarda estado, y cada cuantos kb se respalda

    public BackupProperties(File fileInfo) throws IOException {
        this.fileInfo = fileInfo;
        props = new Properties();

        if (fileInfo.exists())
            loadData();
        else {
            if (!fileInfo.getParentFile().exists())
                fileInfo.getParentFile().mkdirs();
            fileInfo.createNewFile();
            saveData();
        }
    }

    public BackupProperties(File folder, String fileName) throws IOException {
        this(new File(folder, fileName));
    }

    private void saveData() {
        try {
            props.store(new FileOutputStream(fileInfo), "BackupInfo");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadData() {
        try {
            props.load(new FileInputStream(fileInfo));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteFile() {
        fileInfo.delete();
    }

    public String getProperty(String key) {
        return props.getProperty(key);
    }

    public void setProperty(String key, String value) {
        props.setProperty(key, value);
        saveData();
    }

    public void removeProperty(String key) {
        props.remove(key);
        saveData();
    }

}
