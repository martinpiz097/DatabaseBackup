package org.dbbackup.backup;


import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import static org.dbbackup.backup.PropertyKeys.ACTIVE;
import static org.dbbackup.backup.PropertyKeys.DBNAME;

public class BackupManager {
    private LinkedList<Backup> listBackups;
    private File managerFolder;
    private BackupProperties properties;

    public BackupManager() throws IOException {
        listBackups = new LinkedList<>();
        this.managerFolder = new File("backups");
        if (!managerFolder.exists())
            managerFolder.mkdirs();
        loadBackups();
        properties = new BackupProperties(managerFolder, "manager.properties");
    }

    private void loadBackups() throws IOException {
       File[] backupsDirs = managerFolder.listFiles(f->f.isDirectory());
        if (backupsDirs != null) {
            File dir;
            for (int i = 0; i < backupsDirs.length; i++) {
                dir = backupsDirs[i];
                listBackups.add(new Backup(managerFolder, dir.getName()));
            }
        }
    }

    public String getActiveDatabase() {
        return properties.getProperty(DBNAME);
    }

    public void setActiveDatabase(String dbName) {
        properties.setProperty(DBNAME, dbName);
    }

    public Backup getActiveBackup() {
        if (!isActive())
            return null;
        return listBackups.parallelStream().filter(b->b.getDatabaseName()
                .equals(getActiveDatabase())).findFirst().orElse(null);
    }

    public void addBackup(Backup backup) {
        listBackups.removeIf(b->b.getDatabaseName().equals(backup.getDatabaseName()));
        listBackups.add(backup);
    }

    public void removeBackup(String dbName) {
        listBackups.removeIf(b->b.getDatabaseName().equals(dbName));
    }

    public LinkedList<Backup> getListBackups() {
        return listBackups;
    }

    public File getManagerFolder() {
        return managerFolder;
    }

    public BackupProperties getProperties() {
        return properties;
    }

    public boolean isActive() {
        return Boolean.parseBoolean(properties.getProperty("active"));
    }

    public void active(String dbName) {
        properties.setProperty(ACTIVE, String.valueOf(true));
        properties.setProperty(DBNAME, dbName);
    }

    public void inactive() {
        properties.setProperty(ACTIVE, String.valueOf(false));
        properties.removeProperty(DBNAME);
    }

    public boolean hasBackups() {
        return !listBackups.isEmpty();
    }

    public boolean existsBackup(String dbName) {
        return listBackups.parallelStream().anyMatch(b->b
                .getDatabaseName().equals(dbName));
    }

    public Backup getOrCreateBackup(String dbName) throws IOException {
        Backup backup = getBackup(dbName);
        if (backup == null) {
            backup = new Backup(managerFolder, dbName);
            listBackups.add(backup);
        }
        return backup;
    }

    public Backup getBackup(String dbName) {
        if (!hasBackups())
            return null;
        return listBackups.parallelStream().filter(b->b.getDatabaseName().equals(dbName))
                .findFirst().orElse(null);
    }

}
