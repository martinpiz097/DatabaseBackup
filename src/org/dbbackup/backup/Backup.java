package org.dbbackup.backup;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

import static org.dbbackup.backup.PropertyKeys.*;

public class Backup implements Serializable {
    private File backupFolder;
    private File dumpFile;
    private BackupProperties properties;

    // rescatar nombrebd, cuota, cantidad de respaldos, fechaUltimo respaldo

    public Backup(File managerFolder, String dbName) throws IOException {
        this.backupFolder = new File(managerFolder, dbName);

        if (backupFolder.exists()) {
            properties = new BackupProperties(backupFolder, "info.properties");
            String dumpPath = getDumpPath();
            String dumpName = getDumpName();
            if (dumpPath != null && dumpName != null) {
                dumpFile = new File(dumpPath, dumpName);
            }
        }
        else {
            backupFolder.mkdirs();
            properties = new BackupProperties(backupFolder, "info.properties");
            setDatabaseName(dbName);
        }
    }

    public String getDatabaseName() {
        return properties.getProperty(DBNAME);
    }

    public void setDatabaseName(String dbName) {
        properties.setProperty(DBNAME, dbName);
    }

    public double getQuoteInKb() {
        String property = properties.getProperty(QUOTE);
        if (property ==  null)
            return 0;
        return Double.parseDouble(property);
    }

    public void setQuote(double kb) {
        properties.setProperty(QUOTE, String.valueOf(kb));
    }

    public void addBackupCount() {
        int bkpCount = getBackupCount();
        System.out.println("BpkCount: "+bkpCount);
        System.out.println("NewCount: "+(++bkpCount));
        properties.setProperty(BACKUPCOUNT, String.valueOf(bkpCount));
    }

    public int getBackupCount() {
        String property = properties.getProperty(BACKUPCOUNT);
        if (property ==  null)
            return 0;
        return Integer.parseInt(property);
    }

    public String getDumpPath() {
        return properties.getProperty(DUMPPATH);
    }

    public void setDumpPath(String dumpPath) {
        properties.setProperty(DUMPPATH, dumpPath);
        //dumpFile = new File(dumpPath, getDumpName());
    }

    public String getDumpName() {
        return properties.getProperty(DUMPNAME);
    }

    public void setDumpName(String dumpName) {
        properties.setProperty(DUMPNAME, dumpName);
        dumpFile = new File(getDumpPath(), dumpName);
    }

    public String getState() {
        int backupCount = getBackupCount();
        return backupCount > 0 ? "Respaldado el "+getBackupDate()
                : "No respaldado nunca";
    }

    public String getBackupDate() {
        String property = properties.getProperty(BACKUPDATE);
        if (property ==  null)
            return null;
        return property;
    }

    // Ver si se deben cambiar las fechas
    public void setBackupDate(Date date) {
        properties.setProperty(BACKUPDATE, date.toString());
    }

    public void setBackupDate(String date) {
        properties.setProperty(BACKUPDATE, date);
    }

    public File getBackupFolder() {
        return backupFolder;
    }

    public void setBackupFolder(File backupFolder) {
        this.backupFolder = backupFolder;
    }

    public File getDumpFile() {
        return dumpFile;
    }

    public void setDumpFile(File dumpFile) {
        this.dumpFile = dumpFile;
    }

    public BackupProperties getProperties() {
        return properties;
    }

    public void setProperties(BackupProperties properties) {
        this.properties = properties;
    }
}
