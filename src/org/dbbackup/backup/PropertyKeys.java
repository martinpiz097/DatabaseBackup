package org.dbbackup.backup;

public class PropertyKeys {
    public static final String DBNAME = "dbName";
    public static final String QUOTE = "kbQuote";
    public static final String BACKUPDATE = "bkpDate";
    public static final String BACKUPCOUNT = "bkpCount";
    public static final String DUMPPATH = "dumpPath"; // Verificar si no existe
    public static final String DUMPNAME = "dumpName"; // Verificar si no existe
    public static final String STATE = "state"; // Verificar si no existe
    public static final String ACTIVE = "active";

}
