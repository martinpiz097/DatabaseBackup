package org.dbbackup.gui.model;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.util.LinkedList;

public class CBMDatabases implements ComboBoxModel<String> {

    private final LinkedList<String> listDatabases;
    private String selected;

    public CBMDatabases(LinkedList<String> listDatabases) {
        this.listDatabases = listDatabases;
    }

    public int getIndex(String object) {
        int count = 0;
        for (String str : listDatabases) {
            if (str.equals(object))
                return count;
            count++;
        }
        return -1;
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selected = anItem == null ? null : anItem.toString();
    }

    @Override
    public Object getSelectedItem() {
        return selected;
    }

    @Override
    public int getSize() {
        return listDatabases.size();
    }

    @Override
    public String getElementAt(int index) {
        return listDatabases.get(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {

    }

    @Override
    public void removeListDataListener(ListDataListener l) {

    }

}
