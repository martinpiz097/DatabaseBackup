package org.dbbackup.gui;

import org.dbbackup.backup.Backup;
import org.dbbackup.backup.BackupManager;
import org.dbbackup.backup.TBackupMonitor;
import org.dbbackup.db.DBConnection;
import org.dbbackup.gui.model.CBMDatabases;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;

public class FormBackup extends JFrame {
    private JPanel mainPanel;
    private JPanel panelDatabase;
    private JTextField txtHost;
    private JTextField txtUsername;
    private JPasswordField txtPassw;
    private JButton btnConnect;
    private JPanel panelBackup;
    private JComboBox cboDatabase;
    private JSpinner spinbu;
    private JTextField txtBackupDir;
    private JButton btnExplore;
    private JLabel lblMissing;
    private JLabel lblUnit;
    private JPanel panelBottom;
    private JLabel lblMonitor;
    private JButton btnEnable;
    private JButton btnDisable;
    private JButton btnChangeFilter;
    private JLabel lblState;

    private DBConnection connection;
    private BackupManager manager;
    private Backup current;
    private TBackupMonitor monitor;

    public FormBackup() throws IOException {
        connection = new DBConnection();
        manager = new BackupManager();
        //monitor = new TBackupMonitor(manager, connection);
        current = manager.getActiveBackup();
    }

    private JFileChooser getConfiguredFileChooser() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.isDirectory();
            }

            @Override
            public String getDescription() {
                return "Solo directorios";
            }
        });
        fileChooser.setMultiSelectionEnabled(false);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        return fileChooser;
    }

    private void importBackupData() {
        try {
            CBMDatabases cboModel = (CBMDatabases) cboDatabase.getModel();
            cboDatabase.setSelectedIndex(cboModel.getIndex(current.getDatabaseName()));
            spinbu.setValue(current.getQuoteInKb());
            txtBackupDir.setText(current.getDumpPath());
            lblState.setText(current.getState());
            lblMonitor.setText(manager.isActive()?"ACTIVO":"INACTIVO");
            lblMissing.setText(new DecimalFormat("#0.00").format(connection.getMissing(current.getDatabaseName(),
                    current.getQuoteInKb())));
            mainPanel.updateUI();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void initBackup() {
        btnEnable.setEnabled(current == null);
        btnDisable.setEnabled(current != null);
        if (current != null) {
            importBackupData();
            monitor = new TBackupMonitor(manager, connection, lblMissing);
            monitor.start();
        }
    }

    private void enablePanel(JPanel panel, boolean enable, JComponent... exceptions) {
        Component[] components = panel.getComponents();

        // Ver que se setee el no respaldado nunca habiendo una archivo ya almacenado
        for (int i = 0; i < components.length; i++)
            components[i].setEnabled(enable);

        if (exceptions != null)
            for (int i = 0; i < exceptions.length; i++)
                exceptions[i].setEnabled(!enable);
    }

    private void init() {
        initComponents();
        initListeners();
    }

    private void initComponents() {
        setTitle("Backup Managment");
        spinbu.setValue(1.0);
        this.setContentPane(mainPanel);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocation(400, 200);
        this.pack();

        txtBackupDir.setEditable(false);
        enablePanel(panelBackup, false);
        setVisible(true);
    }

    private void initListeners() {
        btnConnect.addActionListener(e -> {
            String host = txtHost.getText().trim();
            String userName = txtUsername.getText().trim();
            String passw = String.valueOf(txtPassw.getPassword());

            if (host.isEmpty() || userName.isEmpty() || passw.isEmpty()) {
                JOptionPane.showMessageDialog(this, "Faltan datos por ingresar",
                        "Error", JOptionPane.ERROR_MESSAGE);
                txtHost.selectAll();
                txtHost.requestFocus();
            }
            else {
                try {
                    connection.open(host, userName, passw);
                    txtHost.setEnabled(false);
                    txtUsername.setEnabled(false);
                    txtPassw.setEnabled(false);
                    btnConnect.setEnabled(false);
                    cboDatabase.setModel(new CBMDatabases(connection.getDatabases()));
                    //cboDatabase.setSelectedIndex(0);
                    cboDatabase.updateUI();

                    enablePanel(panelDatabase, false);
                    enablePanel(panelBackup, true);
                    initBackup();

                } catch (SQLException e1) {
                    JOptionPane.showMessageDialog(this,
                            "Error de conexión: "+e1.getMessage(),
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtHost.selectAll();
                    txtHost.requestFocus();
                }
            }

        });

        btnExplore.addActionListener(e -> {
            JFileChooser fileChooser = getConfiguredFileChooser();
            fileChooser.showOpenDialog(this);

            File dirSelected = fileChooser.getSelectedFile();
            if (dirSelected != null) {
                txtBackupDir.setText(dirSelected.getPath());
                btnEnable.setEnabled(!txtBackupDir.getText().isEmpty());
            }
        });

        cboDatabase.addActionListener(e -> {
            String dbName = (String) cboDatabase.getSelectedItem();
            if (dbName != null) {
                if (!(current != null && current
                        .getDatabaseName().equals(dbName))){
                    try {
                        current = manager.getOrCreateBackup(dbName);
                        importBackupData();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        btnChangeFilter.addActionListener(e -> {
            if (current != null) {
                current.setQuote(Double.parseDouble(spinbu.getValue().toString()));
                try {
                    lblMissing.setText(String.valueOf(connection.getMissing(current
                            .getDatabaseName(), current.getQuoteInKb())));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });

        btnEnable.addActionListener(e -> {
            String folderPath = txtBackupDir.getText();
            if (folderPath.isEmpty()) {
                JOptionPane.showMessageDialog(this, "Debe definir una ruta para guardar" +
                        "el respaldo");
            }
            else {
                manager.active(current.getDatabaseName());
                manager.addBackup(current);
                current = manager.getActiveBackup();
                System.out.println("Current: "+current);
                current.setDumpPath(folderPath);
                importBackupData();
                monitor = new TBackupMonitor(manager, connection, lblMissing);
                monitor.start();
                btnEnable.setEnabled(false);
                btnDisable.setEnabled(true);
            }
        });

        btnDisable.addActionListener(e -> {
            manager.inactive();
            btnEnable.setEnabled(true);
            btnDisable.setEnabled(false);
        });
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                new FormBackup().init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

}
